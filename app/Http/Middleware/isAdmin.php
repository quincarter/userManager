<?php

namespace App\Http\Middleware;

use Closure;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if (is_object($user)) {
            if ($user->role->name === 'Admin' ||
                $user->role->name === 'User') {
                return $next($request);
            }
        }

        return redirect('/');
    }
}
