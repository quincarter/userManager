<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class isLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()) {
            $user = $request->user();
            if (is_object($user)) {
                if ($user->role->name === 'Admin' ||
                    $user->role->name === 'User') {
                    return $next($request);
                }
            }
        }

        return redirect('/');
    }
}
