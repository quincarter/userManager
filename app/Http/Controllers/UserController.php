<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        if(!Auth::user()) {
            return view('auth.login');
        } else {
            return redirect('/users');
        }
    }
    public function displayUsers()
    {
        $users = User::orderBy('id', 'ASC')->latest()->get();
        return view('users.index', compact('users'));
    }

    public function editUsers($id)
    {
        $singleUser = User::findOrFail($id);
        return view('users.edit', compact('singleUser'));
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $user = User::findOrFail($id);
        try {
            $user->update($input);
            return redirect('/users');
        } catch (\Exception $e) {
            $singleUser = User::findOrFail($id);
            $singleUser['errors'] = 'Either another user already exists with the email specified, or there was something wrong with the data entered in the form. Please try again. ';
            return view('users.edit', compact('singleUser'));
        }
    }
}
