<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index');

Auth::routes();

Route::get('/users', 'UserController@displayUsers')->middleware('loggedIn');
Route::get('/users/{id}/edit', 'UserController@editUsers')->middleware('admin');
Route::post('/users/{id}/save', 'UserController@update')->middleware('admin')->name('save');