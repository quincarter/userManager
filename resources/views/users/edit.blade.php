@extends('layouts.app')

@section('content')
    <edit-users-component
            :single-user="{{ json_encode($singleUser) }}"
            :is-admin="{{ Auth::user()->role_id }}">
    </edit-users-component>
@endsection
